

const items = ['hello', 'world', 23, '23', null , true ];
document.write('<br> ---- ДЗ ---- - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних. - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив [hello, world, 23, 23, null], і другим аргументом передати string, то функція поверне масив [23, null]');
document.write('<br>Варіант 1 <br>');
function filterBy(arr, type) {
    const textRe = [ ];
    for (let i=0; i < arr.length; i++) {
        if ( typeof arr[i] !== type) {
            if ( arr[i] === null ){
                textRe.push('null');
            }
            else {
                textRe.push(arr[i]);  
            }
        }
    }
    return textRe;
}

document.write(filterBy(items,"string"));
document.write('<hr>'); 

document.write('<br>Варіант 2 <br>');
function filterBy1(arr1, type1) {
    return arr1.filter(function (arr1) {
        if(typeof arr1 !== type1){
        return  arr1;        
        } 
    })
}
document.write(filterBy1(items, "string"));
document.write('<hr>'); 

document.write('<br>Варіант 3 <br>');

const filterBy2 = (arr, type) => arr.filter(item => typeof item !== type)

document.write(filterBy2(items, "string"));
document.write('<hr>'); 



// Создаем массив со словами
const words = [
    "программа",
    "макака",
    "прекрасный",
    "оладушек"
];
// Выбираем случайное слово
const word = words[Math.floor(Math.random() * words.length)];
// Создаем итоговый массив
let answerArray = [];
for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
}
let remainingLetters = word.length;
// Игровой цикл
while (remainingLetters > 0) {
// Показываем состояние игры
alert(answerArray.join(" "));
// Запрашиваем вариант ответа
let guess = prompt("Угадайте букву, или нажмите Отмена для выхода из игры.");
if (guess === null) {
    // Выходим из игрового цикла
    break;
    } 
    else if (guess.length !== 1) {
    alert("Пожалуйста, введите одиночную букву.");
    } 
    else {
    // Обновляем состояние игры
        for (let j = 0; j < word.length; j++) {
        if (word[j] === guess) {
            answerArray[j] = guess;
            remainingLetters--;
            }
        }
    }
// Конец игрового цикла
}
// Отображаем ответ и поздравляем игрока
alert(answerArray.join(" "));
alert(`Отлично! Было загадано слово ${word}`);